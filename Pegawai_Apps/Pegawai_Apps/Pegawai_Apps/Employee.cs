﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pegawai_Apps
{
    class Employee
    {
        [PrimaryKey, AutoIncrement]
        public long Empid { get; set; }
        [NotNull]
        public string EmpName { get; set; }
        public string Designaiton { get; set; }
        public string Department { get; set; }
        public string Qualificaiton { get; set; }
    }
}
