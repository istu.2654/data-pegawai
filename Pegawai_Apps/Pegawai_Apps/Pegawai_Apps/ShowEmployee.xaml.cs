﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pegawai_Apps
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShowEmployee : ContentPage
    {
        private Employee currEmp;
        public ShowEmployee(Employee employee)
        {
            InitializeComponent();
            currEmp = employee;
            BindingContext = currEmp;
        }
        public async void OnEditClicked(object sender, EventArgs args)
        {
            currEmp.EmpName = txtEmpName.Text;
            currEmp.Department = txtDepartment.Text;
            currEmp.Designation = txtDesignation.Text;
            currEmp.Qualification = txtQualification.Text;
            App.DBUtils.EditEmployee(currEmp);
            await Navigation.PopAsync();
        }

    }
}